import {Text, View, TouchableOpacity, Image} from 'react-native';
import React from 'react';


const ContactsItem = ({item, showContact}) => (
    <TouchableOpacity onPress={() => showContact(item.id)}>
        <View style={{
            padding: 20,
            marginTop: 10,
            height: 150,
            backgroundColor: '#2b2b2b',
            alignItems: 'center'
        }}

        >
            <Image
                source={{uri: item.image}}
                style={{width: 50, height: 50, marginRight: 10, borderRadius: 2}}
            />
            <Text style={{marginTop: 5, fontSize: 18, color: '#fff'}}>{item.name}</Text>
            <Text style={{marginTop: 5, fontSize: 18, color: '#fff'}}>email: {item.email}</Text>
            <Text style={{marginTop: 5, fontSize: 18, color: '#fff'}}>mobile: {item.mobile}</Text>
        </View>
    </TouchableOpacity>
);

export default ContactsItem;
