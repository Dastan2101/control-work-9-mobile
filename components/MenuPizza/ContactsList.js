import React from 'react';
import {StyleSheet, View, FlatList, TouchableOpacity, Text, Modal, Image} from 'react-native';
import {getContacts, showContact} from "../../store/actions/actionCreator";
import {connect} from "react-redux";
import ContactsItem from "./MenuItem/ContactsItem";

class ContactsList extends React.Component {

    state = {
        modalVisible: false,
    };


    showModal = (id) => {
        this.props.showContact(id);
        this.setState({modalVisible: true, content: this.props.contact});
    };

    hideModal = () => {
        this.setState({modalVisible: false})
    };

    componentDidMount() {
        this.props.getContacts()
    }

    render() {

        return (
            <View style={styles.container}>
                <FlatList
                    data={this.props.contacts}
                    renderItem={({item}) => <ContactsItem showContact={this.showModal} item={item}/>}
                    keyExtractor={(item) => item.id}
                />
                <Modal
                    animationType="slide"
                    visible={this.state.modalVisible}
                    onRequestClose={this.hideModal}>

                    <View style={{marginTop: 10, backgroundColor: '#fff', padding: 5}}>
                        <View style={{flex: 'row'}} onPress={this.hideModal}>
                            <Text>Hello i'm modal</Text>
                        </View>

                        <View style={{
                            padding: 20,
                            marginTop: 10,
                            marginBottom: 10,
                            height: 150,
                            backgroundColor: '#2b2b2b',
                            alignItems: 'center'
                        }}

                        >
                            <Image
                                source={{uri: this.props.contact.image}}
                                style={{width: 50, height: 50, marginRight: 10, borderRadius: 2}}
                            />
                            <Text style={{marginTop: 5, fontSize: 18, color: '#fff'}}>{this.props.contact.name}</Text>
                            <Text style={{
                                marginTop: 5,
                                fontSize: 18,
                                color: '#fff'
                            }}>email: {this.props.contact.email}</Text>
                            <Text style={{
                                marginTop: 5,
                                fontSize: 18,
                                color: '#fff'
                            }}>mobile: {this.props.contact.mobile}</Text>
                        </View>

                        <TouchableOpacity style={styles.btn} onPress={this.hideModal}>
                            <Text style={{fontSize: 30, fontWeight: 'bold'}}
                            >Back to list</Text>
                        </TouchableOpacity>
                    </View>

                </Modal>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'stretch',
        justifyContent: 'flex-start',
        paddingHorizontal: 5,
        paddingTop: 30
    },
    btn: {
        padding: 20,
        marginTop: 10,
        height: 70,
        backgroundColor: '#180aff',
        flexDirection: 'row',
        alignItems: 'center'
    },

});

const mapStateToProps = state => ({
    contacts: state.contacts,
    contact: state.contact,

});

const mapDispatchToProps = dispatch => ({
    getContacts: () => dispatch(getContacts()),
    showContact: (id) => dispatch(showContact(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactsList)