import {initContact, initFailure, initRequest, initSuccess} from "./actionTypes";
import axios from "../../axios-contacts";


export const getContacts = () => {
    return dispatch=> {
        dispatch(initRequest());
        axios.get('contacts.json').then(response => {
            dispatch(initSuccess(response.data));
        },error => {
            dispatch(initFailure(error))
        })
    }
};

export const showContact = (id) => {
    return dispatch => {
        dispatch(initRequest());
        axios.get('contacts/' + id + '.json').then(response => {
            dispatch(initContact(response.data))
        },error => {
            dispatch(initFailure(error))
        })
    }
};

