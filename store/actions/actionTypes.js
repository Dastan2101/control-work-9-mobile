export const INIT_REQUEST = 'INIT_REQUEST';
export const INIT_SUCCESS = 'INIT_SUCCESS';
export const INIT_FAILURE = 'INIT_FAILURE';

export const INIT_CONTACT = 'INIT_CONTACT';

export const initRequest = () => ({type: INIT_REQUEST});
export const initSuccess = (contacts) => ({type: INIT_SUCCESS,contacts});
export const initFailure = (error) => ({type: INIT_FAILURE, error});

export const initContact = (contact) => ({type: INIT_CONTACT, contact});