import {INIT_CONTACT, INIT_SUCCESS} from "../actions/actionTypes";

const initialState = {
    contacts: [],
    contact: {}

};

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case INIT_SUCCESS:
            let contactsArray = Object.keys(action.contacts).map(id => {
                return {...action.contacts[id], id}
            });

            return {...state, contacts: contactsArray};

        case INIT_CONTACT:
            console.log(action.contact)

            return {
                ...state, contact: action.contact
            };

    }
    return state;
};


export default dishesReducer;